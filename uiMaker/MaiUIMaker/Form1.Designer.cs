﻿
using System;
using System.Windows.Forms;

namespace MaiUIMaker
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.panelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drawTextToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.drawPictureToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.textBox_width = new System.Windows.Forms.TextBox();
            this.textBox_height = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox_x = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_y = new System.Windows.Forms.TextBox();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.新建ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.另存为ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.日志ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.parmLabel = new System.Windows.Forms.Label();
            this.parmText = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.colorBtn = new System.Windows.Forms.Button();
            this.colorDialog1 = new System.Windows.Forms.ColorDialog();
            this.contextMenuStrip1.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.contextMenuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.panelToolStripMenuItem,
            this.drawTextToolStripMenuItem,
            this.drawPictureToolStripMenuItem});
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(70, 70);
            // 
            // panelToolStripMenuItem
            // 
            this.panelToolStripMenuItem.Name = "panelToolStripMenuItem";
            this.panelToolStripMenuItem.Size = new System.Drawing.Size(69, 22);
            // 
            // drawTextToolStripMenuItem
            // 
            this.drawTextToolStripMenuItem.Name = "drawTextToolStripMenuItem";
            this.drawTextToolStripMenuItem.Size = new System.Drawing.Size(69, 22);
            // 
            // drawPictureToolStripMenuItem
            // 
            this.drawPictureToolStripMenuItem.Name = "drawPictureToolStripMenuItem";
            this.drawPictureToolStripMenuItem.Size = new System.Drawing.Size(69, 22);
            // 
            // treeView1
            // 
            this.treeView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.treeView1.LabelEdit = true;
            this.treeView1.Location = new System.Drawing.Point(0, 41);
            this.treeView1.Margin = new System.Windows.Forms.Padding(4);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(191, 290);
            this.treeView1.TabIndex = 2;
            // 
            // textBox_width
            // 
            this.textBox_width.Location = new System.Drawing.Point(101, 345);
            this.textBox_width.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_width.Name = "textBox_width";
            this.textBox_width.Size = new System.Drawing.Size(89, 25);
            this.textBox_width.TabIndex = 3;
            this.textBox_width.TextChanged += new System.EventHandler(this.width_TextChanged);
            // 
            // textBox_height
            // 
            this.textBox_height.Location = new System.Drawing.Point(101, 379);
            this.textBox_height.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_height.Name = "textBox_height";
            this.textBox_height.Size = new System.Drawing.Size(89, 25);
            this.textBox_height.TabIndex = 4;
            this.textBox_height.TextChanged += new System.EventHandler(this.height_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 349);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 15);
            this.label1.TabIndex = 5;
            this.label1.Text = "百分比宽";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 382);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 15);
            this.label2.TabIndex = 6;
            this.label2.Text = "百分比高";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 415);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(60, 15);
            this.label3.TabIndex = 7;
            this.label3.Text = "百分比x";
            // 
            // textBox_x
            // 
            this.textBox_x.Location = new System.Drawing.Point(101, 411);
            this.textBox_x.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_x.Name = "textBox_x";
            this.textBox_x.Size = new System.Drawing.Size(89, 25);
            this.textBox_x.TabIndex = 8;
            this.textBox_x.LostFocus += new System.EventHandler(this.x_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(16, 448);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(60, 15);
            this.label4.TabIndex = 9;
            this.label4.Text = "百分比y";
            // 
            // textBox_y
            // 
            this.textBox_y.Location = new System.Drawing.Point(101, 444);
            this.textBox_y.Margin = new System.Windows.Forms.Padding(4);
            this.textBox_y.Name = "textBox_y";
            this.textBox_y.Size = new System.Drawing.Size(89, 25);
            this.textBox_y.TabIndex = 10;
            this.textBox_y.LostFocus += new System.EventHandler(this.y_TextChanged);
            // 
            // menuStrip1
            // 
            this.menuStrip1.AutoSize = false;
            this.menuStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.menuStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Visible;
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 2);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(533, 35);
            this.menuStrip1.TabIndex = 11;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.新建ToolStripMenuItem,
            this.另存为ToolStripMenuItem,
            this.日志ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(53, 31);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 新建ToolStripMenuItem
            // 
            this.新建ToolStripMenuItem.Name = "新建ToolStripMenuItem";
            this.新建ToolStripMenuItem.Size = new System.Drawing.Size(137, 26);
            this.新建ToolStripMenuItem.Text = "新建";
            this.新建ToolStripMenuItem.Click += new System.EventHandler(this.新建ToolStripMenuItem_Click);
            // 
            // 另存为ToolStripMenuItem
            // 
            this.另存为ToolStripMenuItem.Name = "另存为ToolStripMenuItem";
            this.另存为ToolStripMenuItem.Size = new System.Drawing.Size(137, 26);
            this.另存为ToolStripMenuItem.Text = "另存为";
            this.另存为ToolStripMenuItem.Click += new System.EventHandler(this.另存为ToolStripMenuItem_Click);
            // 
            // 日志ToolStripMenuItem
            // 
            this.日志ToolStripMenuItem.Name = "日志ToolStripMenuItem";
            this.日志ToolStripMenuItem.Size = new System.Drawing.Size(137, 26);
            this.日志ToolStripMenuItem.Text = "日志";
            this.日志ToolStripMenuItem.Click += new System.EventHandler(this.日志ToolStripMenuItem_Click);
            // 
            // parmLabel
            // 
            this.parmLabel.AutoSize = true;
            this.parmLabel.Location = new System.Drawing.Point(16, 481);
            this.parmLabel.Name = "parmLabel";
            this.parmLabel.Size = new System.Drawing.Size(37, 15);
            this.parmLabel.TabIndex = 12;
            this.parmLabel.Text = "参数";
            this.parmLabel.Visible = false;
            // 
            // parmText
            // 
            this.parmText.Location = new System.Drawing.Point(101, 478);
            this.parmText.Margin = new System.Windows.Forms.Padding(4);
            this.parmText.Name = "parmText";
            this.parmText.Size = new System.Drawing.Size(89, 25);
            this.parmText.TabIndex = 13;
            this.parmText.Visible = false;
            this.parmText.TextChanged += new System.EventHandler(this.parmText_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(16, 518);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 15);
            this.label5.TabIndex = 14;
            this.label5.Text = "颜色";
            // 
            // colorBtn
            // 
            this.colorBtn.Location = new System.Drawing.Point(101, 511);
            this.colorBtn.Margin = new System.Windows.Forms.Padding(4);
            this.colorBtn.Name = "colorBtn";
            this.colorBtn.Size = new System.Drawing.Size(91, 29);
            this.colorBtn.TabIndex = 15;
            this.colorBtn.UseVisualStyleBackColor = true;
            this.colorBtn.Click += new System.EventHandler(this.colorBtnDown);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.ClientSize = new System.Drawing.Size(1173, 554);
            this.Controls.Add(this.colorBtn);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.parmText);
            this.Controls.Add(this.parmLabel);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.textBox_y);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.textBox_x);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.textBox_height);
            this.Controls.Add(this.textBox_width);
            this.Controls.Add(this.treeView1);
            this.KeyPreview = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Form_KeyDown);
            this.contextMenuStrip1.ResumeLayout(false);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }














        #endregion
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.ToolStripMenuItem panelToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drawTextToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem drawPictureToolStripMenuItem;
        private TextBox textBox_width;
        private TextBox textBox_height;
        private Label label1;
        private Label label2;
        private Label label3;
        private TextBox textBox_x;
        private Label label4;
        private TextBox textBox_y;
        private MenuStrip menuStrip1;
        private ToolStripMenuItem 文件ToolStripMenuItem;
        private ToolStripMenuItem 新建ToolStripMenuItem;
        private ToolStripMenuItem 另存为ToolStripMenuItem;
        private ToolStripMenuItem 日志ToolStripMenuItem;
        private Label parmLabel;
        private TextBox parmText;
        private Label label5;
        private Button colorBtn;
        private ColorDialog colorDialog1;
    }
}

