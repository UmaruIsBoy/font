﻿using MaiUIMaker.BaseLua;
using MaiUIMaker.tool;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace MaiUIMaker
{
    public partial class Form1 : Form
    {
        private Dictionary<ToolStripItem, TreeNode> menuTreeNodeDic = new Dictionary<ToolStripItem, TreeNode>();
        private Dictionary<TreeNode, Control> nodeControlDic = new Dictionary<TreeNode, Control>();
        private Dictionary<Control, string> controlParmDic = new Dictionary<Control,string>();
        public static Dictionary<Control, Color> controlColorDic = new Dictionary<Control, Color>();
        private TreeView treeView1;
        private Panel mainPanel;
        private Panel defaultPanel;

        private Graphics lastFrame;
        private Color lastColor;
        private LogForm logForm;
        public Form1()
        {
            // Initialize the form.
            InitializeComponent();
            OnRegister();
            Init();

        }

        private void Init()
        {
            logForm=new LogForm();
        }

        private void OnRegister()
        {
            treeView1.AfterLabelEdit += TreeView1_AfterLabelEdit;
            treeView1.AfterSelect += TreeView1_AfterSelect;
            treeView1.NodeMouseClick += TreeView1_NodeMouseClick;
            UserInput.form1 = this;
 
        }



        private void TreeView1_NodeMouseClick(object sender, TreeNodeMouseClickEventArgs e)
        {
            Control control = nodeControlDic[e.Node];
            SetParmText(control);
            SetFocusColor(control);
        }
        private void SetFocusColor(Control control)
        {
            Pen p = new Pen(Color.Red, 4);
            var g = control.CreateGraphics();
            if (lastFrame != default)
            {
                lastFrame.Clear(lastColor);
            }
            lastFrame = g;
            lastColor = control.BackColor;
            g.DrawRectangle(p, 0, 0, control.Width, control.Height);
        }
        private void Start_MouseDown(object sender, MouseEventArgs e)
        {
            var control = sender as Control;
            SetFocusColor(control);
            SetWidthHeightText();
            SetParmText(control);

            //拖动窗体
            if (e.Location.X + 10 < control.Width&& e.Location.X>10&& e.Location.Y >= 10&& e.Location.Y + 10 < control.Height)
            {
                Tools.ReleaseCapture();
                Tools.SendMessage(control.Handle, Tools.WM_SYSCOMMAND, Tools.SC_MOVE + Tools.HTCAPTION, 0);
                x_TextChanged(sender, e);
                y_TextChanged(sender, e);
            }
        }


        private void Start_MouseDown2(object sender, MouseEventArgs e)
        {

            var control = sender as Control;
            if ( Control.MouseButtons == MouseButtons.Left)
            {
                int dir = 0;
                Tools.ReleaseCapture();
                if (e.Location.X + 10 >= control.Width)
                {
                    dir = dir + Tools.HTCAPTION;
                }
                if (e.Location.X < 10)
                {
                    dir = dir + Tools.HTCAPTION_LEFT;
                }
                if (e.Location.Y < 10)
                {
                    dir = dir+ Tools.HTCAPTION_TOP;
                }
                if (e.Location.Y+10 >= control.Height)
                {
                    dir = dir+ Tools.HTCAPTION_DOWN;
                }
                Tools.SendMessage(control.Handle, Tools.WM_SYSCOMMAND, Tools.SC_SIZE + dir, 0);

                SetWidthHeightText();
            }
        }
        private void Control_SizeChanged(object sender, EventArgs e)
        {
            var control = sender as Control;
            SetFocusColor(control);
        }


        private bool NodeChildHasSameName(TreeNode node,TreeNode newNode,string newText)
        {
            if (node==null)
            {
                return false;
            }
            TreeNodeCollection collection = node.Nodes;
            if (collection!=null)
            {
                foreach (TreeNode item in collection)
                {
                    if (item.Text == newText && newNode != item)
                    {
                        return true;
                    }
                }
                return false;
            }
            else
            {
                return false;
            }

        }
        private ContextMenuStrip CreateControlMenu(TreeNode node)
        {
            ContextMenuStrip strip = new ContextMenuStrip();
            var panel=strip.Items.Add("Panel");
            var picture=strip.Items.Add("DrawPicture");
            var text=strip.Items.Add("DrawText");
            var uitext=strip.Items.Add("UIText");
            var uibox=strip.Items.Add("UIBox");
            var delete=strip.Items.Add("删除");
            var top=strip.Items.Add("置顶");
            strip.ItemClicked += Strip_ItemClicked;
            menuTreeNodeDic.Add(panel, node);
            menuTreeNodeDic.Add(picture, node);
            menuTreeNodeDic.Add(text, node);
            menuTreeNodeDic.Add(delete, node);
            menuTreeNodeDic.Add(top, node);
            menuTreeNodeDic.Add(uibox, node);
            menuTreeNodeDic.Add(uitext, node);
            Console.WriteLine(menuTreeNodeDic.Count);


            return strip;
        }

        private void Strip_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {
            string name = e.ClickedItem.Text;
            var treeNode = menuTreeNodeDic[e.ClickedItem];
            var focus = nodeControlDic[treeNode];
            treeView1.SelectedNode = treeNode;
            if (name=="删除")
            {
                //移除数据
                RemoveData(focus);
                //移除树的节点
                treeNode.Remove();
                RemoveTreeNodeDic(treeNode);
                menuTreeNodeDic.Remove(e.ClickedItem);
                if (focus == mainPanel)
                {
                    defaultPanel.Controls.Remove(mainPanel);
                    mainPanel = default;
                    this.Controls.Remove(defaultPanel);
                }
                else
                {
                    focus.Parent.Controls.Remove(focus);
                }
            }
            else if(name=="置顶")
            {
                TreeNode oldNode = treeView1.SelectedNode;
                TreeNode newNode = (TreeNode)oldNode.Clone();
                Control oldControl = nodeControlDic[oldNode];
                if (oldControl==mainPanel)
                {
                    MessageBox.Show("主面板不能置顶", "提示", MessageBoxButtons.OK);
                    return;
                }
                nodeControlDic.Add(newNode, oldControl);
                newNode.ContextMenuStrip = CreateControlMenu(newNode);
                oldNode.Parent.Nodes.Insert(0, newNode);

                SaveTreeNodeData(oldNode, newNode);

                Control newControl = nodeControlDic[newNode];
                newControl.SendToBack();

                oldNode.Parent.Nodes.Remove(oldNode);
                nodeControlDic.Remove(oldNode);
            }
            else
            {
                var newNode = treeNode.Nodes.Add(name + $"_{treeNode.Nodes.Count}");
                newNode.ContextMenuStrip = CreateControlMenu(newNode);
                Control control = CreateControlByName(name);
                Random ran = new Random();
                if (control is Panel)
                {
                    control.BackColor = Color.FromArgb(255, 255, 255,255);
                }
                else
                {
                    control.BackColor = Color.FromArgb(255, ran.Next(255), ran.Next(255), ran.Next(255));
                }
                controlColorDic.Add(control, control.BackColor);
                colorDialog1.Color = control.BackColor;
                nodeControlDic.Add(newNode, control);
                SetParmText(control);
                focus.Controls.Add(control);
                control.Size = new Size(control.Parent.Width / 2, control.Parent.Height / 2);
                control.BringToFront();
                treeView1.SelectedNode = newNode;
                colorBtn.BackColor = default;
               
                control.MouseDown += Start_MouseDown;
                control.MouseDown += Start_MouseDown2;
                control.SizeChanged += Control_SizeChanged;

            }
        }

  
        private void SaveTreeNodeData(TreeNode node,TreeNode newNode)
        {

            foreach (TreeNode oldNode in node.Nodes)
            {
                var oldControl = nodeControlDic[oldNode];
                //TODO
                var newChildNode = newNode.Nodes[oldNode.Index];

                nodeControlDic.Add(newChildNode, oldControl);

                newChildNode.ContextMenuStrip = CreateControlMenu(newChildNode);

                SaveTreeNodeData(oldNode, newNode.Nodes[oldNode.Index]);
                
                nodeControlDic.Remove(oldNode);
            }

        }
        private void RemoveTreeNodeDic(TreeNode treeNode)
        {
            List<ToolStripItem> toolStripItem = new List<ToolStripItem>();
            foreach (var item in menuTreeNodeDic)
            {
                if (item.Value == treeNode)
                {
                    toolStripItem.Add(item.Key);
                }
            }
            foreach (var item in toolStripItem)
            {
                menuTreeNodeDic.Remove(item);
            }
        }
        private void RemoveData(Control control)
        {
            List<TreeNode> treeNodes = new List<TreeNode>();
            control.MouseDown -= Start_MouseDown;
            controlParmDic.Remove(control);
            controlColorDic.Remove(control);
            foreach (var keyValue in nodeControlDic)
            {
                if (keyValue.Value == control)
                {
                    treeNodes.Add(keyValue.Key);
                }
            }
            foreach (var item in treeNodes)
            {
                nodeControlDic.Remove(item);
            }
            foreach (Control child in control.Controls)
            {
                child.MouseDown -= Start_MouseDown;
                controlParmDic.Remove(child);
                controlColorDic.Remove(child);
                foreach (var keyValue in nodeControlDic)
                {
                    if (keyValue.Value == child)
                    {
                        treeNodes.Add(keyValue.Key);
                    }
                }
                foreach (var item in treeNodes)
                {
                    nodeControlDic.Remove(item);
                }
                RemoveData(child);
            }
        }
        private Control CreateControlByName(string name)
        {
            Control control=default;
            if (name == "Panel")
            {
                var panel = new UIPanel();
                return panel;
            }
            else if (name == "UIBox")
            {
                return new UIBox();
            }
            else if (name == "UIText")
            {
                return new UIText();
            }
            else if (name == "DrawPicture")
            {
                var picture = new DrawPicture();
                return picture;
            }
            else if (name == "DrawText")
            {
                var text = new DrawText();
                return text;
            }
            return control;
        }



        private void TreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            SetWidthHeightText();
            parmText_TextChanged(this, new EventArgs());

        }
        public void SetWidthHeightText()
        {
            var control = nodeControlDic[treeView1.SelectedNode];
            if (control.Parent != default)
            {
                textBox_width.Text = Convert.ToString(control.Width / (float)control.Parent.Width);
                textBox_height.Text = Convert.ToString(control.Height / (float)control.Parent.Height);
                textBox_x.Text = Convert.ToString(control.Location.X / (float)control.Parent.Width);
                textBox_y.Text = Convert.ToString(control.Location.Y / (float)control.Parent.Height);
            }
            else
            {
                textBox_width.Text = Convert.ToString(control.Width);
                textBox_height.Text = Convert.ToString(control.Height);
                textBox_x.Text = Convert.ToString(control.Location.X);
                textBox_y.Text = Convert.ToString(control.Location.Y);
            }
        }

        private void width_TextChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            try
            {
                float x = Convert.ToSingle(textBox.Text);
                if (treeView1.SelectedNode != default)
                {
                    Control control=nodeControlDic[treeView1.SelectedNode];
                    if (control.Parent != default)
                    {
                        x = control.Parent.Width * x;
                    }
                    control.Size = new Size((int)x, control.Size.Height);
                }
            }
            catch (Exception)
            {
                textBox.Clear();
            }
       
        }

        private void height_TextChanged(object sender, EventArgs e)
        {
            var textBox = sender as TextBox;
            try
            {
                float y = Convert.ToSingle(textBox.Text);
                Control control = nodeControlDic[treeView1.SelectedNode];
                if (control.Parent != default)
                {
                    y = y * control.Parent.Height;
                }
                control.Size = new Size(control.Size.Width, (int)y);

            }
            catch (Exception)
            {
                textBox.Clear();
            }
        }


        private void TreeView1_AfterLabelEdit(object sender, NodeLabelEditEventArgs e)
        {
            var treeView=sender as TreeView;
            TreeNode parentNode= e.Node.Parent;
            string text = e.Label;
            bool hasSameName= NodeChildHasSameName(parentNode, e.Node, text);
            if(hasSameName)
            {
                MessageBox.Show("同一层级不能重名", "提示", MessageBoxButtons.OK);
            }
            e.CancelEdit = hasSameName;
        }

        public void x_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double x = Convert.ToDouble(textBox_x.Text);
                if (treeView1.SelectedNode != default)
                {
                    Control control = nodeControlDic[treeView1.SelectedNode];
                    if (control.Parent != default)
                    {
                        x = control.Parent.Width * x;
                    }
                    if (sender as TextBox!=null)
                    {
                        control.Location = new Point((int)x, control.Location.Y);
                    }
                    else
                    {
                        TreeView1_AfterSelect(sender, e as TreeViewEventArgs);
                    }
                }
            }
            catch (Exception)
            {
                textBox_x.Clear();
            }
        }

        public void y_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double y = Convert.ToDouble(textBox_y.Text);
                Control control = nodeControlDic[treeView1.SelectedNode];
                if (control.Parent != default)
                {
                    y = y * control.Parent.Height;
                }
                if (sender as TextBox != null)
                {
                    control.Location = new Point(control.Location.X, (int)y);
                }
                else
                {
                    TreeView1_AfterSelect(sender, e as TreeViewEventArgs);
                }
            }
            catch (Exception)
            {
                textBox_y.Clear();
            }
        }

        private void 新建ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int x = treeView1.Location.X + treeView1.Size.Width;
            int y = treeView1.Location.Y;
            if (mainPanel == default)
            {
                Point panelPos = new Point(x, y);
                defaultPanel = new Panel();
                defaultPanel.Size = new Size(1920/2, 1080/2);
                defaultPanel.Location = panelPos;
                defaultPanel.BackColor = Color.CadetBlue;
                this.Controls.Add(defaultPanel);

                TreeNode treeNode = treeView1.Nodes.Add("main_panel");
                treeNode.ContextMenuStrip = CreateControlMenu(treeNode);

                mainPanel = new Panel();
                mainPanel.Size = new Size(1920/2, 1080/2);
                mainPanel.BackColor = Color.White;
                nodeControlDic.Add(treeNode, mainPanel);
                defaultPanel.Controls.Add(mainPanel);
                defaultPanel.BringToFront();
                mainPanel.BringToFront();


                textBox_width.Text = "1";
                textBox_height.Text = "1";
            }
            else
            {
                MessageBox.Show("不能反复创建", "提示", MessageBoxButtons.OK);
            }
        }

        private void 另存为ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            logForm.textBox1.Clear();
            LogChild(treeView1.Nodes);
            logForm.Show();
            logForm.TopMost = true;
        }
        private void LogChild(TreeNodeCollection treeNodes,string parentName=default)
        {
            if (parentName==default)
            {
                parentName = "";
            }
            foreach (TreeNode item in treeNodes)
            {
                Control control = nodeControlDic[item];
                if (control as Panel == mainPanel)
                {
                    logForm.textBox1.AppendText(CreatePanel(control, item.Text) + "\n");
                }
                else
                {
                    logForm.textBox1.AppendText(AddControl(control, item.Text, parentName) + "\n");
                }
                if (item.Nodes.Count > 0)
                {
                    string name;
                    if (item.Parent==null)
                    {
                        name = item.Text;
                    }
                    else
                    {
                        if (control.Parent as Panel ==mainPanel)
                        {
                            name = item.Parent.Text + "." + item.Text;
                        }
                        else
                        {
                            name = parentName + "." + item.Text;
                        }
                
                    }
                    LogChild(item.Nodes, name);
                }
            }

        }

        private void 日志ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            logForm.Show();
            logForm.TopMost = true;
        }

        private string CreatePanel(Control control,string name)
        {
            string rst="";
            if (control is UIPanel)
            {
                LuaPanel luaPanel = new LuaPanel();
                if (control==mainPanel)
                {
                    rst = luaPanel.CreateMainPanel(name, control.Location.X, control.Location.Y);
                }
                else
                { 
                    rst= luaPanel.CreatePanel(name, control.Location.X, control.Location.Y, control.Width,control.Height);
                }
            }
            return rst;
        }
        private string AddControl(Control control, string name,string parentName)
        {
            Control parent = control.Parent;
            LuaPanel luaPanel = new LuaPanel();
            string setName = controlParmDic[control];
            string rst = luaPanel.AddControl(name,parentName, control, setName,
            control.Location.X / (float)parent.Width,
            control.Location.Y / (float)parent.Height,
            control.Width / (float)parent.Width,
            control.Height / (float)parent.Height);
            return rst;
        }
        private void SetParmText(Control control)
        {
            parmLabel.Visible = true;
            parmText.Visible = true;
            if (control is DrawPicture)
            {
                parmLabel.Text = "图片名称";
            }
            if (control is DrawText)
            {
                parmLabel.Text = "文字";
            }
            if (control is Panel || control is UIBox)
            {
                parmLabel.Visible = false;
                parmText.Visible = false;
            }


            if (controlColorDic.ContainsKey(control))
            {
                colorBtn.BackColor = controlColorDic[control];
            }


        }

        private void colorBtnDown(object sender, EventArgs e)
        {
            colorDialog1.ShowDialog();
            colorBtn.BackColor=colorDialog1.Color;
            if (treeView1.SelectedNode!=null)
            {
                var control = nodeControlDic[treeView1.SelectedNode];
      
                controlColorDic[control] = colorDialog1.Color;
                
                control.BackColor = colorDialog1.Color;
            }

        }
        public Control GetSelectControl()
        {
            if (treeView1.SelectedNode==null)
            {
                return null;
            }
            return nodeControlDic[treeView1.SelectedNode];
        }

        private void Form_KeyDown(object sender, KeyEventArgs e)
        {
            UserInput.KeyDown(sender, e);
        }

        private void parmText_TextChanged(object sender, EventArgs e)
        {
            if (treeView1.SelectedNode == null) return;
            Control control = nodeControlDic[treeView1.SelectedNode];
            if (sender==this)
            {
                if (!controlParmDic.ContainsKey(control))
                {
                    controlParmDic.Add(control, "");
                    parmText.Text = "";
                }
                else if(control is DrawText && parmText.Text != controlParmDic[control])
                {
                    parmText.Text = controlParmDic[control];

                }
                return;
            }
        
    
            if (control is DrawText)
            {
                if (!controlParmDic.ContainsKey(control))
                {
                    controlParmDic.Add(control, "");
                    parmText.Text = "";
                }
                else if (parmText.Text != controlParmDic[control])
                {
                    controlParmDic[control] = parmText.Text;
                }
            }
        }
    }
}
