﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaiUIMaker.tool
{
    public static class Tools
    {
        [DllImport("user32.dll")]//拖动无窗体的控件
        public static extern bool ReleaseCapture();
        [DllImport("user32.dll")]
        public static extern bool SendMessage(IntPtr hwnd, int wMsg, int wParam, int lParam);
        public const int WM_SYSCOMMAND = 0x0112;
        public const int SC_MOVE = 0xF010;
        public const int HTCAPTION = 0x0002;
        public const int HTCAPTION_LEFT = 0x0001;
        public const int HTCAPTION_TOP = 0x0003;
        public const int HTCAPTION_DOWN = 0x0006;
        public const int SC_SIZE = 0xF000;

        [DllImport("user32.dll", SetLastError = true)]
        public static extern int GetWindowLong(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", CharSet = CharSet.Auto, ExactSpelling = true)]
        public static extern IntPtr GetForegroundWindow();
        [DllImport("user32.dll")]//拖动无窗体的控件
        public static extern bool MoveWindow(IntPtr hwnd, int X,int Y,int nWidth, int nHeight,bool bRepaint);
    }

}
