﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaiUIMaker
{
    static class Program
    {
        [STAThreadAttribute()]
        static void Main()
        {
            Application.Run(new Form1());
        }
    }
}
