﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaiUIMaker
{
    public partial class LogForm : Form
    {
        public LogForm()
        {
            InitializeComponent();
            this.FormClosing += LogForm_FormClosing;
        }


        private void LogForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            e.Cancel = true;
            this.Visible = false;
        }
    }
}
