﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaiUIMaker
{
    public static class UserInput
    {
        internal static Form1 form1;

        public static void KeyDown(object sender, KeyEventArgs e)
        {
            Control control = form1.GetSelectControl();
            if (control==null)
            {
                return;
            }
            int y = control.Location.Y;
            int x = control.Location.X;

            if (e.KeyCode==Keys.W)
            {
                y -= 1;
            }

            if (e.KeyCode == Keys.S)
            {
                y += 1;
            }
            if (e.KeyCode == Keys.A)
            {
                x -= 1;
            }
            if (e.KeyCode == Keys.D)
            {
                x += 1;
            }
            control.Location = new Point(x, y);
        }
    }
}
