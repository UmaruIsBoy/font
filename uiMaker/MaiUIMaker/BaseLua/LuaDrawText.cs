﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaiUIMaker.BaseLua
{
    public class LuaDrawText:BaseControl
    {
        private string cmdCreate = "DrawText:Create()";
        private string cmdAdd = "AddPanel";
        public string CreateText(string name, string text)
        {
            return $"{cmdCreate}:Set(text={text})";
        }
        public string AddPanel(string name, string parentName, int x, int y, float width, float height)
        {
            CreateArgs(name, x, y, width, height);
            return $"{parentName}.{name}={parentName}:{cmdAdd} {Args}";
        }

    }
}
