﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MaiUIMaker.BaseLua
{
    public abstract class BaseControl
    {
        private string args = "(0,0,0,0)";

        public BaseControl()
        {
        }
        public void CreateArgs(string name, float x, float y, float width, float height) {
            Args = $"(\"{name}\",{x},{y},{width},{height})";
        }
        public void CreateMainPanelArgs(string name, float x, float y)
        {
            Args = $"(\"{name}\",{x},{y},UI.ScreenSize().width,UI.ScreenSize().height)";
        }

        public void CreateItemArgs(string name, string parmName, float x, float y, float width, float height)
        {
            Args = $"(\"{name}\",\"{parmName}\",{x},{y},{width},{height})";
        }
        public void AddRGBArgs(Color color)
        {
            Args=Args.Insert(Args.Length-1, $",{color.R},{color.G},{color.B}");
        }

        public string Args { get => args; set => args = value; }
    }
}
