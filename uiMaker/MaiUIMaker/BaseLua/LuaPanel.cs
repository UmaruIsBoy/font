﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MaiUIMaker.BaseLua
{
    public class LuaPanel : BaseControl
    {
        private string cmdCreate= "Panel:New";
        private Dictionary<Type, string> ControlTypeCmdDic=new Dictionary<Type, string>(4);

        public LuaPanel()
        {
            ControlTypeCmdDic[typeof(UIPanel)] = "AddPanel";
            ControlTypeCmdDic[typeof(DrawPicture)] = "AddPicture";
            ControlTypeCmdDic[typeof(DrawText)] = "AddText";
            ControlTypeCmdDic[typeof(UIBox)] = "AddUIBox";
            ControlTypeCmdDic[typeof(UIText)] = "AddUIText";

        }
        public string CreatePanel(string name, int x, int y, int width, int height)
        {
            CreateArgs(name, x, y, width, height);
            return $"{name}={cmdCreate} {Args}";
        }
        public string CreateMainPanel(string name, int x, int y)
        {
            CreateMainPanelArgs(name, x, y);
            return $"{name}={cmdCreate} {Args}";
        }
        public string AddControl(string name,string parentName,Control control,string setName, float x, float y, float width, float height)
        {
            var type = control.GetType();
            string cmd = ControlTypeCmdDic[type];
            
            if ((control is Panel)||(control is UIBox))
            {
                CreateArgs(name, x, y, width, height);
            }
            else
            {
                CreateItemArgs(name, setName, x, y, width, height);
            }
            if (Form1.controlColorDic[control]!=default)
            {
                AddRGBArgs(Form1.controlColorDic[control]);
            }
           
            return $"{parentName}:{cmd} {Args}";
        }
    }
}
