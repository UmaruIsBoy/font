
--存放文字数据
Draw.Texts={
    ['测']={{0,1,1,1,0,5,1,1,0,10,1,1,1,2,2,1,1,6,2,1,1,9,1,1,2,1,5,1,2,3,1,6,2,11,1,1,3,10,1,1,4,3,1,7,5,10,1,1,6,2,1,7,6,11,1,1,8,2,1,7,9,11,2,1,10,0,1,12},{11,12,17}},
    ['试']={{0,0,1,1,0,4,2,1,1,1,1,1,1,5,1,7,2,10,1,1,4,2,7,1,4,5,3,1,4,10,2,1,5,6,1,5,6,9,1,1,8,0,1,10,9,10,2,1,10,0,1,1,10,9,1,3},{11,12,14}},
    --字不存在时会用空格代替，11代表空格的宽，12代表空格的高
    [' ']={{},{11,12,0}},
}
Draw.Pictures={
    ['book']={{{{71,35,20},{0,0,2,2}},{{133,75,39},{0,2,2,1}},{{79,37,21},{0,36,2,2}},{{121,77,50},{2,0,1,2}},{{133,84,58},{2,2,1,1}},{{82,40,22},{2,3,1,1}},{{90,52,19},{2,35,2,1,25,35,2,1}},{{161,113,76},{2,36,2,2,25,36,2,1}},{{163,115,80},{3,0,1,3,27,0,1,3}},{{84,44,15},{3,3,1,1}},{{115,69,40},{3,4,1,3,26,4,1,3}},{{167,123,63},{3,7,1,1}},{{180,136,73},{3,8,3,2,3,14,4,6,3,20,3,12,4,10,2,22,6,12,1,8,6,27,2,5,7,17,1,3,7,32,2,1,8,29,3,2,8,31,1,2}},{{180,136,75},{3,10,1,2}},{{180,136,70},{3,12,1,2}},{{178,134,74},{3,32,1,1,20,17,3,3}},{{138,84,45},{3,33,22,2}},{{161,122,82},{4,0,3,3,25,0,2,3}},{{80,53,17},{4,3,23,1}},{{154,102,57},{4,4,22,3}},{{182,138,78},{4,7,22,1,10,16,5,1,16,12,1,3,20,16,4,1,22,15,2,2}},{{177,133,70},{4,32,3,1,19,19,1,1,23,8,2,2}},{{55,28,4},{4,35,21,1}},{{170,133,73},{4,36,21,2}},{{179,143,79},{6,8,2,2}},{{174,133,69},{6,10,2,2}},{{174,130,67},{6,20,2,4,12,8,3,2,25,20,2,4}},{{153,109,43},{6,24,2,1}},{{177,130,61},{6,25,2,2}},{{169,129,85},{7,0,18,3}},{{182,140,82},{7,12,1,3}},{{180,135,79},{7,15,1,1}},{{180,136,76},{7,16,1,1}},{{182,142,74},{8,8,2,2}},{{184,142,71},{8,10,2,2}},{{178,134,71},{8,12,2,3,17,10,6,2}},{{179,137,79},{8,15,2,1}},{{174,134,66},{8,16,2,1}},{{171,127,64},{8,17,2,2}},{{188,148,77},{8,19,2,1}},{{166,118,56},{8,20,2,3}},{{172,128,62},{8,23,2,1}},{{176,129,59},{8,24,2,1}},{{160,113,47},{8,25,2,2}},{{191,147,84},{8,27,2,2}},{{179,135,72},{9,31,16,2,11,29,14,4,12,27,13,6,14,23,11,10,16,19,1,14,17,21,8,12,19,20,6,13,23,19,2,14,24,18,1,15}},{{191,151,88},{10,8,2,2}},{{187,143,83},{10,10,2,2}},{{175,131,71},{10,12,2,3,18,17,1,1}},{{177,133,69},{10,15,2,1,12,10,3,2}},{{177,138,72},{10,17,1,2}},{{185,140,71},{10,19,1,1}},{{197,157,86},{10,20,1,3}},{{172,124,62},{10,23,1,1}},{{164,116,56},{10,24,1,1}},{{168,124,61},{10,25,1,2}},{{178,133,74},{10,27,2,2}},{{177,133,67},{11,17,4,3,25,24,2,1}},{{173,131,59},{11,20,1,3}},{{186,153,78},{11,23,3,4}},{{176,131,74},{12,12,3,3}},{{168,124,60},{12,15,3,1}},{{186,142,76},{12,20,3,3}},{{200,164,98},{15,8,1,2}},{{173,129,69},{15,10,2,2}},{{182,138,72},{15,12,1,3}},{{185,145,79},{15,15,2,2}},{{182,133,68},{15,17,1,2}},{{189,147,76},{15,19,1,1}},{{166,124,52},{15,20,1,3}},{{176,132,69},{16,8,1,2}},{{179,135,75},{16,17,1,2}},{{181,139,79},{17,8,3,2}},{{200,156,90},{17,12,2,3}},{{176,132,72},{17,15,3,2,17,17,1,4,18,18,2,1,18,19,1,2,19,17,1,2}},{{181,137,71},{19,12,1,3}},{{186,142,82},{20,8,3,2}},{{181,137,74},{20,12,4,3}},{{183,139,79},{20,15,2,1}},{{184,140,80},{23,10,2,2}},{{172,128,68},{23,17,1,2}},{{176,132,66},{24,12,3,4}},{{180,140,72},{24,16,1,1}},{{181,137,77},{24,17,1,1}},{{181,136,79},{25,8,2,2}},{{174,130,70},{25,10,2,2}},{{178,145,97},{25,16,2,1}},{{196,154,82},{25,17,2,3}},{{169,125,65},{25,25,2,2}},{{159,115,52},{25,27,2,2}},{{159,105,51},{25,29,2,3}},{{132,81,39},{25,32,2,1}},{{84,44,18},{25,33,2,2}},{{164,116,81},{25,37,2,1}},{{168,123,72},{26,7,1,1}},{{80,38,22},{27,3,1,1}},{{69,36,17},{27,35,1,1}},{{130,81,55},{27,36,1,2}},{{90,38,26},{28,0,1,2}},{{37,12,0},{28,2,1,1}},{{53,20,1},{28,36,1,2}},},{29,38,140}}
}

--逐渐放大的书本
--myPicture=DrawPicture:Create()
--myPicture:Set({picture="book",width=50,height=100,x=UI.ScreenSize().width/2,y=UI.ScreenSize().height/2})
--myPicture:Adaptation("center")
--myPicture:DoScale({width=50,height=100},{width=300,height=500},10)
--
--myText=DrawText:Create()
--myText:Set({text="测试",x=UI.ScreenSize().width/2,y=UI.ScreenSize().height/2,width=20,height=60})

--myText:DisPlayMode("vertical")
--myText:Adaptation("center")

--myText:Set({x=0,y=0})
--myText:DoMove({x=0,y=0},{x=UI.ScreenSize().width/2,y=UI.ScreenSize().height/2},2)
--myText:BlinkShow(1)  文字逐一显示功能，暂时不能和Do系列联用
--myText:DoColor({r=255,g=255,b=255},{r=0,g=0,b=0},10)
--myText:DoScale({width=20,height=60},{width=500,height=20},10)





--从生成器中导入
--
--main_panel=Panel:New ("main_panel",0,0,UI.ScreenSize().width,UI.ScreenSize().height)
--main_panel:AddUIBox ("UIBox_1",0.2177083,0.1759259,0.5,0.5,82,201,236)
--main_panel:AddText ("DrawText_1","测试",0.2541667,0.2203704,0.4322917,0.3555556,34,91,206)
--main_panel:DoMove({x=0,y=0},{x=UI.ScreenSize().width/2,y=UI.ScreenSize().height/2},2)
--main_panel:DoScale({width=20,height=40},{width=100,height=20},10)
--main_panel:Set({x=UI.ScreenSize().width/2,y=UI.ScreenSize().height/2})


CollisionComponent = {
    name = "CollisionComponent"
}

function CollisionComponent:New()
    local component = {}
    setmetatable(component, {__index=self})
    component.velocity = {x=0,y=0,z=0}
    component.gravity = 0.1
    component.onFloor = true
    return component
end


function CollisionComponent:Set(args)
    self.weight = args.weight or self.weight
    self.gravity = args.gravity or self.gravity
end
function CollisionComponent:Get()
    return {
        weight=self.weight,
        gravity= self.gravity
    }
end
function CollisionComponent:CheckCollision(other)
    local box1Set = self.box:Get()
    local box2Set = other:Get()

    -- 计算box1和box2的碰撞盒
    local box1Collider = {x=box1Set.x, y=box1Set.y, width=box1Set.width, height=box1Set.height}
    local box2Collider = {x=box2Set.x, y=box2Set.y, width=box2Set.width, height=box2Set.height}

    -- 检测碰撞
    if box1Collider.x < box2Collider.x + box2Collider.width and
            box1Collider.x + box1Collider.width > box2Collider.x and
            box1Collider.y < box2Collider.y + box2Collider.height and
            box1Collider.y + box1Collider.height > box2Collider.y then
        return true
    else
        return false
    end
end

function CollisionComponent:ResolveCollision(other)
    local box1Set = self.box:Get()
    local box2Set = other:Get()

    -- 计算box1和box2的碰撞盒
    local box1Collider = {x=box1Set.x, y=box1Set.y, width=box1Set.width, height=box1Set.height}
    local box2Collider = {x=box2Set.x, y=box2Set.y, width=box2Set.width, height=box2Set.height}

    -- 计算最近的未碰撞位置
    local overlapX = math.max(0, math.min(box1Collider.x + box1Collider.width, box2Collider.x + box2Collider.width) - math.max(box1Collider.x, box2Collider.x))
    local overlapY = math.max(0, math.min(box1Collider.y + box1Collider.height, box2Collider.y + box2Collider.height) - math.max(box1Collider.y, box2Collider.y))
    local overlap = math.min(overlapX, overlapY)

    -- 恢复到最近的未碰撞位置

    if overlap > 0 then
        if overlap == overlapX then
            if box1Collider.x < box2Collider.x then
                self.box:Set({x=box1Set.x - 1*overlap})
                other:Set({x=box2Set.x + 0*overlap})
            else
                self.box:Set({x=box1Set.x + 1*overlap})
                other:Set({x=box2Set.x - 0*overlap})
            end
        else
            if box1Collider.y < box2Collider.y then
                self.box:Set({y=box1Set.y - 1*overlap})
                other:Set({y=box2Set.y + 0*overlap})
            else
                self.box:Set({y=box1Set.y + 1*overlap})
                other:Set({y=box2Set.y - 0*overlap})
            end
        end
    end
end




Framework.UIPlug.OnUpdate:Register(function()
    for cur, data in pairs(UI.BoxesMgr) do
        -- 添加重力
        local comp=cur:GetComponent(CollisionComponent)
        if comp.gravity>0 then
            comp.velocity.y = comp.velocity.y + comp.gravity
            -- 更新box的位置
            cur:Set({x=cur:Get().x + comp.velocity.x, y=cur:Get().y + comp.velocity.y})

        end
    end

    for cur, data in pairs(UI.BoxesMgr) do
        -- 检测碰撞
        for other, data in pairs(UI.BoxesMgr) do
            if cur ~= other and cur:HasComponent(CollisionComponent) then
                local collisionComponent = cur:GetComponent(CollisionComponent)
                if collisionComponent:CheckCollision(other) then
                    collisionComponent:ResolveCollision(other)
                    collisionComponent.velocity.y=-collisionComponent.gravity
                    other:Set({r=255,g=0,b=0})
                    if collisionComponent.onFloor==false then
                        collisionComponent.onFloor=true
                    end
                else
                    other:Set({r=255,g=255,b=255})
                end
            end
        end

    end
end)

--创建可控制box
local screen=UI.ScreenSize()
box1 = UI.Box.Create()
box1:Set({x=screen.width/2, y=screen.height/2, width=20, height=20})
local comp=CollisionComponent:New()
comp:Set({gravity=0.1})
box1:AddComponent(comp)

--创建平台
box2 = UI.Box.Create()
box2:Set({x=0, y=screen.height/2+20, width=screen.width, height=20})

comp=CollisionComponent:New()
comp:Set({gravity=0})
box2:AddComponent(comp)

Framework.KeyDown.SPACE:Register(function()
    local comp=box1:GetComponent(CollisionComponent)
    if comp.onFloor==true then
        comp.velocity.y = comp.velocity.y - 7
        comp.onFloor=false
    end
end)
Framework.KeyInput.D:Register(function()
    local comp=box1:GetComponent(CollisionComponent)
    box1:Set({x=box1:Get().x+1})

end)
Framework.KeyInput.A:Register(function()
    local comp=box1:GetComponent(CollisionComponent)
    box1:Set({x=box1:Get().x-1})
end)